#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

from flask import Flask, render_template, request, flash, redirect, url_for, jsonify, session
import sys
from flask_paginate import Pagination, get_page_parameter

import requests
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api, Resource
import json
import logging
from logging import Formatter, FileHandler
import os
from pprint import pprint

from flask_login import LoginManager, UserMixin, login_required, login_user, logout_user, current_user

from sklearn.feature_extraction.text import CountVectorizer
from mlmodels import fakenews_model, fakenews_vocab, classify_article
import collections


from flask_locale import Locale, _
from langconfig import LANGUAGES

from datetime import datetime

#----------------------------------------------------------------------------#
# App Config.
#----------------------------------------------------------------------------#

app = Flask(__name__)
app.config.from_object('config')
app.config['LANGUAGES'] = LANGUAGES

db = SQLAlchemy(app) #initialize DB
migrate = Migrate(app, db)
# import models
from forms import *
from models import *


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': models.User}


login_manager = LoginManager(app)
keywords = dict()
#ogin_manager.init_app(app)


app.config['LOCALE_PATH'] = 'translations'
locale = Locale(app)

ARTICLE_DATEFORMAT = '%Y-%m-%dT%H:%M:%SZ'
IMPERIAL_DATEFORMAT = '%m/%d/%y'
METRIC_DATEFORMAT = '%d/%m/%y'

#login_manager.init_app(app)

# silly user model
# class User(UserMixin):
#
#     def __init__(self, id, name, password):
#         self.id = id
#         self.name = name
#         self.password = password
#
#     def __repr__(self):
#         return "%s/%s" % (self.name, self.password)

#users = [User('1', 'dog', 'dog')]


#----------------------------------------------------------------------------#
# Controllers.
#----------------------------------------------------------------------------#

#
# 'home' routes
#
@app.route('/', methods=['GET', 'POST'])
def home():
    # add logic to check if user is logged in,
    # if logged in redirect to home page, else show home page
    if current_user.is_authenticated:
        return redirect(url_for('home_page'))
    else:
        return redirect(url_for('login'))

@app.route('/homepage', methods=['GET'])
@login_required
def home_page():
    sorted_x = sorted(keywords.items(), key=lambda kv: kv[1], reverse=True)
    sorted_dict = collections.OrderedDict(sorted_x)
    if current_user.is_authenticated:
        return render_template('./pages/home.html', data=sorted_dict)
    else:
        return redirect(url_for('login'))


@app.route('/about', methods=['GET'])
def about():
    return render_template('pages/placeholder.about.html', authenticated=current_user.is_authenticated)

#
# 'authentication' routes
#
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if current_user.is_authenticated:
        return redirect(url_for('home_page'))
    # form = LoginForm(request.form)
    # if form.validate_on_submit():
    #     user = User.query.filter_by(name=form.name.data).first()
    #     print('no')
    #     if user is None or not user.check_password(form.password.data):
    #         flash('Invalid username or password')
    #         #return redirect(url_for('login'))
    #         return user
    #     login_user(user, remember=form.remember_me.data)
    #     return redirect(url_for('home_page'))
    # return render_template('forms/login.html', form=form)
    if request.method == 'GET':
        title = "Fake News - News Stream with fake news spotter"
        return render_template('forms/login.html', title=title , form=form)
    elif request.method == 'POST':
        # check if user is in db <--- needs to actually use db lol
        name = form.name.data
        password = form.password.data # <-- lmao hash this
        user = models.User.query.filter_by(name=name).first()

        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user)
        return redirect(url_for('home_page'))
        #if form.validate_on_submit():
            # logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
            # logging.debug('This is a log message.')
            # flash('Invalid username or password')

        # for user in users:
        #     # if user provides correct username and password
        #     if user.name == username and user.password == password:
        #         login_user(user)
        #         return redirect(url_for('home_page'))

        #flash(u'Invalid password provided', 'primary')
        #return render_template('forms/login.html', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    # form = RegisterForm(request.form)
    # return render_template('forms/register.html', form=form)
    if current_user.is_authenticated:
        return redirect(url_for('home_page'))
    form = RegisterForm(request.form)
    if request.method == 'GET':
        return render_template('forms/register.html', form=form)
    elif request.method == 'POST':
        name = form.name.data
        email = form.email.data
        test = models.User.query.filter_by(name=name).first()
        if test is not None:
            flash('Please use a different username.')
            return redirect(url_for('register'))
        test2 = models.User.query.filter_by(email=email).first()
        if test2 is not None:
            flash('Please use a different email.')
            return redirect(url_for('register'))
        user = models.User(name=name, email=email)
        if user is None:
            flash('Please Fill Out Form')
            return redirect(url_for('register'))
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))

@app.route('/forgot', methods=['GET', 'POST'])
def forgot():
    form = ForgotForm(request.form)
    global data
    if request.method == 'GET':
        return render_template('forms/forgot.html', form=form)
    elif request.method == 'POST':
        email = form.email.data
        test = models.User.query.filter_by(email=email).first()
        if test is None:
            flash('Not A Valid Email in System')
            return redirect(url_for('forgot'))
        ###### MUST FIGURE OUT HOW TO PASS EMAIL FROM HERE TO RESET
        if email is None:
            flash('Please Enter an Email')
            return redirect(url_for('forgot'))
        data = email
        return redirect(url_for('reset'))


@app.route('/reset', methods=['GET', 'PUT', 'POST'])
def reset():
    if data is None:
        return redirect(url_for('forgot'))
    user = models.User.query.filter_by(email=data).first()

    form = ResetForm(request.form)
    if request.method == 'GET':
        return render_template('forms/reset.html', form=form)
    elif request.method == 'PUT' or request.method == 'POST':
        password = form.password.data
        user.set_password(password)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, your password has been reset')
        return redirect(url_for('login'))
        # return password

@app.route('/explore', methods=['GET'])
def explore():
    form = ExploreForm(request.form)
    if request.method == 'GET':
        return render_template('forms/explore.html', form=form, authenticated=current_user.is_authenticated)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@login_manager.user_loader
def load_user(id):
    return models.User.query.get(int(id))
# # callback to reload the user object
# @login_manager.user_loader
# def load_user(userid):
#     print("user_Loader")
#     print(userid)
#     print(type(userid))
#     for user in users:
#         if user.id == userid:
#             print("match")
#             return user
#     return None


@app.route('/search', methods=['GET'])
def search():
    search = ArticleSearchForm(request.form)

    '''
    if request.method == 'POST':
        return search_results(search)
    '''
    return render_template('forms/search.html', form=search, default_language=get_locale())

@app.route('/results')
def search_results(search):
    results = []
    searchs = False
    search_string = search.data['search']
    url = ('https://newsapi.org/v2/everything?'
       'q=' + search_string + '&'
       'from=2019-04-29&'
       'sortBy=popularity&'
       'apiKey=8e407ce1ed494cffb1bb22da0d9a237c&'
       'page=1')
    results = requests.get(url)
    #pprint(json.loads(results.text))
    if results:
        searchs = True

    # page = request.args.get(get_page_parameter(), type=int, default=1)
    # count = results.json()['totalResults']
    # pagination = Pagination(page=page, total=int(count), search=searchs, record_name='articles')

    # print(int(count), file=sys.stderr)
    #
    #
    # # if search.data['search'] == 'hello':
    # #     print('Hello,', file=sys.stderr)
    #
    #     ###Need to use REST API here to queary ALL artciles###
    #
    #
    #
    #     # qry = db_session.query(Album)
    #     # results = qry.all()
    # print("YO", search_string, file=sys.stderr)
    # results = {
    #         "name": "json2html",
    #         "description": "Converts JSON to HTML tabular representation"
    #     }
    if not results:
        flash('No results found!')
        return redirect('/search')
    else:
        # display results
        # print(search_string, file=sys.stderr)
        return render_template('forms/results.html', results=json.loads(results.text)['articles'])#, pagination=pagination)

@locale.localeselector
def get_locale():
    # if the user has set up the language manually it will be stored in the session,
    # so we use the locale from the user settings
    try:
        language = session['language']
    except KeyError:
        language = None
    if language is not None:
        return language
    return 'en' #request.accept_languages.best_match(app.config['LANGUAGES'].keys())

@app.context_processor
def inject_conf_var():
    return dict(
                AVAILABLE_LANGUAGES=app.config['LANGUAGES'],
                CURRENT_LANGUAGE=session.get('language',request.accept_languages.best_match(app.config['LANGUAGES'].keys())))

@app.route('/language/<language>')
def set_language(language=None):
    session['language'] = language
    return redirect(url_for('home'))

#
# Error handlers.
#
@app.errorhandler(500)
def internal_error(error):
    #db_session.rollback()
    return render_template('errors/500.html'), 500

@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404

if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

#----------------------------------------------------------------------------#
# REST API for Fake-News API
#----------------------------------------------------------------------------#

@app.route('/api/predict', methods=['POST'])
def predict():
    '''Fetch article text from request'''
    json_data = request.get_json(force=True)
    print(json_data)
    if not json_data or not 'text' in json_data.keys():
        return jsonify({'response': 'invalid call, must specify json in the following format=> {"text": <insert text here>}'}), 400

    article_text = [json_data['text']]

    '''Vectorize text for prediction'''
    vectorizer = CountVectorizer(
        analyzer = 'word',
        lowercase = False,
        vocabulary = fakenews_vocab
    )

    features = vectorizer.fit_transform(
        article_text
    )

    ret_obj = fakenews_model.predict(features)
    return jsonify({'response': str(ret_obj[0])}), 200

@app.route('/api/search', methods=['POST'])
def search_api():
    json_data = request.get_json(force=True)
    if not json_data or not 'search' in json_data.keys():
        return jsonify({'response': 'invalid call, must specify json in the following format=> {"search": <insert search term here>, "page": <page number>, "pageSize":<number of results per page>}'}), 400

    search_string = json_data['search']
    page = json_data['page']
    pageSize = json_data['pageSize'];
    language = json_data['language']
    domain = json_data['domain'];

    url = ('https://newsapi.org/v2/everything?'
       'q=' + search_string + '&'
       'from=2019-04-29&'
       'sortBy=popularity&'
       'apiKey='+ app.config['NEWS_API_KEY'] + '&'
       'pageSize=' + str(pageSize) +'&'
       'page=' + str(page) + '&'
       'language=' + language
       )

    if domain != "":
        url += "&domains=" + domain

    pprint(url)

    #pprint(url)
    response = requests.get(url).json()
    #pprint(response)
    if search_string not in keywords:
        if (len(keywords) < 20):
            keywords[search_string] = 1
        else:
            key_min = min(keywords.keys(), key=(lambda k: keywords[k]))
            if keywords[key_min] == 1:
                del keywords[key_min]
                keywords[search_string] = 1
    else:
        keywords[search_string] += 1
    pprint(keywords)

    '''
    # Sample Response
    response = {'articles': [{'author': 'Josh Ocampo',
                   'content': 'I spend my weekends looking at photos of dogs.\r\n'
                              'On rescue websites, Ill filter for specific breeds '
                              'like golden retrievers or beagles, in the hopes '
                              'that Ill find the so-called perfect dog to adopt '
                              'who sheds little and sleeps at regular hours. But '
                              'as Ive learne… [+3214 chars]',
                   'description': 'I spend my weekends looking at photos of dogs. '
                                  'Read more...',
                   'publishedAt': '2019-04-30T17:30:00Z',
                   'source': {'id': None, 'name': 'Lifehacker.com'},
                   'title': 'Adopt a Dog, Not a Breed',
                   'url': 'https://lifehacker.com/adopt-a-dog-not-a-breed-1834415902',
                   'urlToImage': 'https://i.kinja-img.com/gawker-media/image/upload/s--UXdRU__r--/c_fill,fl_progressive,g_center,h_900,q_80,w_1600/e4gcw3aqk8qrwjpz2hyh.jpg'},
                  {'author': 'Emily Price',
                   'content': 'If youve been having dreams of an Airplane! '
                              'marathon or have been hoping to catch up on the '
                              'season finales of Jane the Virgin, Scandal, or '
                              'Greys Anatomy, then May is the month for you on '
                              'Hulu.\r\n'
                              'Both Airplane! And Airplane II: The Sequelarrived '
                              'on Hulu on May … [+11292 chars]',
                   'description': 'If you’ve been having dreams of an Airplane! '
                                  'marathon or have been hoping to catch up on the '
                                  'season finales of Jane the Virgin, Scandal, or '
                                  'Grey’s Anatomy, then May is the month for you '
                                  'on Hulu. Read more...',
                   'publishedAt': '2019-05-03T15:00:00Z',
                   'source': {'id': None, 'name': 'Lifehacker.com'},
                   'title': "What's Coming to Hulu, Amazon Prime Video, and HBO in "
                            'May 2019',
                   'url': 'https://lifehacker.com/whats-coming-to-hulu-amazon-prime-video-and-hbo-in-ma-1834503140',
                   'urlToImage': 'https://i.kinja-img.com/gawker-media/image/upload/s--qcVRLrq8--/c_fill,fl_progressive,g_center,h_900,q_80,w_1600/jn2a3hxuvh0caglg2sov.jpg'},
                  {'author': 'Jeffrey Van Camp',
                   'content': 'This story is part of a collection of pieces on how '
                              'we spend money today.\r\n'
                              'With a few keystrokes you can find almost anything '
                              'on Amazon ... and buy it with as little as a single '
                              'click. Its a wonderbox of capitalism. Twenty '
                              'minutes ago I typed in yak for no go… [+14170 '
                              'chars]',
                   'description': 'Amazon is a mucky mess of ads, unknown sellers, '
                                  'misleading sales, and specious information. '
                                  'Defend your dollars with these shopping tips '
                                  'and tricks.',
                   'publishedAt': '2019-04-29T11:00:00Z',
                   'source': {'id': 'wired', 'name': 'Wired'},
                   'title': 'Pro Tips for Shopping Safe on Amazon',
                   'url': 'https://www.wired.com/story/how-shop-safe-amazon/',
                   'urlToImage': 'https://media.wired.com/photos/5cc21aab72d12e53d7c491cd/191:100/pass/SPEND-Amazon-How-To.jpg'},
                  {'author': 'Kenneth R. Rosen',
                   'content': 'It is winter. Along the Iraq-Syria border, Iraqi '
                              'patrol forces have swapped their hard tactical '
                              'helmets for the warmth of beanie caps. The soldiers '
                              'look out from their observation towers, across a '
                              'stretch of desert into Syria.\r\n'
                              'From this concrete tower on the… [+27418 chars]',
                   'description': 'As America argues about the security of the '
                                  'nation’s southern border, Iraq and Syria '
                                  'grapple with a wall of their own, one that’s '
                                  'keeping people safe—and tearing them apart. One '
                                  'writer journeys across the divide.',
                   'publishedAt': '2019-05-02T11:00:00Z',
                   'source': {'id': 'wired', 'name': 'Wired'},
                   'title': 'What Iraq and Syria Can Teach the US About Walls',
                   'url': 'https://www.wired.com/story/the-wall-journey-across-divide-iraq-syria/',
                   'urlToImage': 'https://media.wired.com/photos/5cc92e74baf0a36e85079b1a/191:100/pass/Backchannel-Iraq-Syria-Wall-019.jpg'},
                  {'author': 'Kate Clark',
                   'content': 'Menlo Ventures was founded in 1976 but it took 35 '
                              'years for the venture capital firm to hit the '
                              'jackpot.\r\n'
                              'Since the dot-com boom, Menlo Ventures has teetered '
                              'between good and great. A prolific Silicon Valley '
                              'investor, it’s never quite reached the heights of '
                              'A… [+16164 chars]',
                   'description': 'How Menlo Ventures landed a stake in Uber worth '
                                  'billions.',
                   'publishedAt': '2019-04-29T20:45:23Z',
                   'source': {'id': 'techcrunch', 'name': 'TechCrunch'},
                   'title': 'Getting a piece of Uber',
                   'url': 'http://techcrunch.com/2019/04/29/getting-a-piece-of-uber/',
                   'urlToImage': 'https://techcrunch.com/wp-content/uploads/2019/06/uber-cars-arrows.png?w=753'},
                  {'author': 'Catie Keck',
                   'content': 'You ever see a good boy ring a doorbell? If not, '
                              'you gotta see this.\r\n'
                              'A strange looking dog on the loose was recently '
                              'caught on camera giving a South Carolina homeowner '
                              'the ol ding-dong-ditch treatment in a neighborhood '
                              'in Myrtle Beach.\r\n'
                              'According to local AB… [+843 chars]',
                   'description': 'You ever see a good boy ring a doorbell? If '
                                  'not, you gotta see this. Read more...',
                   'publishedAt': '2019-05-04T04:30:00Z',
                   'source': {'id': None, 'name': 'Gizmodo.com'},
                   'title': "Odd-Looking Dog Attempts to Ring Homeowner's Doorbell",
                   'url': 'https://gizmodo.com/odd-looking-dog-attempts-to-ring-homeowners-doorbell-1834520993',
                   'urlToImage': 'https://i.kinja-img.com/gawker-media/image/upload/s--LdlDDxoB--/c_fill,fl_progressive,g_center,h_900,q_80,w_1600/ftyr98liick99xdh83vp.png'},
                  {'author': 'George Dvorsky',
                   'content': 'Bones from an extinct short-faced bear and a '
                              'wolf-life carnivore are among the many fascinating '
                              'remains pulled from a submerged cave in Mexico. The '
                              'discoveries provide a glimpse into the kinds of '
                              'animals that lived in Central America during the '
                              'last Ice Age.\r'
                              '… [+4677 chars]',
                   'description': 'Bones from an extinct short-faced bear and a '
                                  'wolf-life carnivore are among the many '
                                  'fascinating remains pulled from a submerged '
                                  'cave in Mexico. The discoveries provide a '
                                  'glimpse into the kinds of animals that lived in '
                                  'Central America during the last Ice Age. …',
                   'publishedAt': '2019-05-02T15:50:00Z',
                   'source': {'id': None, 'name': 'Gizmodo.com'},
                   'title': 'Treasure Trove of Ice Age Animal Remains Found in '
                            'Submerged Mexican Cave',
                   'url': 'https://gizmodo.com/treasure-trove-of-ice-age-animal-remains-found-in-subme-1834473611',
                   'urlToImage': 'https://i.kinja-img.com/gawker-media/image/upload/s--kVf-o5Y_--/c_fill,fl_progressive,g_center,h_900,q_80,w_1600/anvci2yz8qlaclu39p6y.jpg'},
                  {'author': 'Morgan Sung',
                   'content': "Look, it's been a long week, and then only thing I "
                              'can think about right now is Tiny Shrek demolishing '
                              'a dog obstacle course.\xa0\r\n'
                              'Sure, the Westminster Dog Show is impressive, but '
                              'can any of those pure breeds beat Tiny Shrek? The '
                              'answer is no. Nothing can compe… [+1131 chars]',
                   'description': "Look, it's been a long week, and then only "
                                  'thing I can think about right now is Tiny Shrek '
                                  'demolishing a dog obstacle course. Sure, the '
                                  'Westminster Dog Show is impressive, but can any '
                                  'of those pure breeds beat Tiny Shrek? The '
                                  'answer is no. Nothing can compete…',
                   'publishedAt': '2019-05-04T00:39:02Z',
                   'source': {'id': 'mashable', 'name': 'Mashable'},
                   'title': 'The immense joy of watching Tiny Shrek sprint through '
                            'an obstacle course',
                   'url': 'https://mashable.com/article/tiny-shrek-dog-obstacle-course/',
                   'urlToImage': 'https://mondrian.mashable.com/2019%252F05%252F04%252F0d%252F88b12ae15d11401dad9116557d58b70f.d3761.png%252F1200x630.png?signature=F-JzHrpRTHYFH5xzVaxYsHdoGZk='},
                  {'author': 'Shannon Connellan',
                   'content': "As if Queer Eye's Fab Five hadn't already made open "
                              'faucets of your tear ducts over three seasons, '
                              "they've gone and done a makeover on a gorgeous "
                              'little rescue dog.\r\n'
                              "I'm not fine. You won't be fine.\r\n"
                              'Created for National Adopt A Shelter Pet Day, the '
                              'mini epis… [+418 chars]',
                   'description': "As if Queer Eye 's Fab Five hadn't already made "
                                  'open faucets of your tear ducts over three '
                                  "seasons, they've gone and done a makeover on a "
                                  "gorgeous little rescue dog. I'm not fine. You "
                                  "won't be fine. Created for National Adopt A "
                                  'Shelter Pet Day, the mini episo…',
                   'publishedAt': '2019-05-01T06:21:01Z',
                   'source': {'id': 'mashable', 'name': 'Mashable'},
                   'title': "'Queer Eye' doing a rescue dog makeover will melt you "
                            "into a puddle o' goo",
                   'url': 'https://mashable.com/video/queer-eye-rescue-dog-makeover/',
                   'urlToImage': 'https://mondrian.mashable.com/2019%252F05%252F01%252Fde%252Fd1a7c5cf29b14a129d4098cfaa6b105c.983ca.png%252F1200x630.png?signature=wBCEXW4qfFUtvo-Qzh01bqX2swQ='},
                  {'author': 'Dylan Haas',
                   'content': 'Building a smart home is a fun, albeit complicated '
                              'undertaking. There are just so many different ways '
                              'to expand your repertoire that it can actually '
                              'become a stressful battle of prioritization. Should '
                              'you hook up a smart speaker\r\n'
                              ' system for your all your ent… [+1142 chars]',
                   'description': 'Building a smart home is a fun, albeit '
                                  'complicated undertaking. There are just so many '
                                  'different ways to expand your repertoire that '
                                  'it can actually become a stressful battle of '
                                  'prioritization. Should you hook up a smart '
                                  'speaker system for your all your enter…',
                   'publishedAt': '2019-05-02T15:40:09Z',
                   'source': {'id': 'mashable', 'name': 'Mashable'},
                   'title': 'Philips Hue smart lighting starter kit is almost $30 '
                            'off on Amazon',
                   'url': 'https://mashable.com/shopping/deal-may-2-philips-hue-smart-lighting-starter-kit-amazon/',
                   'urlToImage': 'https://mondrian.mashable.com/2019%252F05%252F02%252F4d%252F24fb7e3295fd4601956fa4cb389f20ef.e8aa0.png%252F1200x630.png?signature=lsFAHPnhS_y46LttUehsOfGIhiA='}],
     'status': 'ok',
     'totalResults': 4987}
    '''

    current_locale = get_locale()
    dateformat_to_use = ARTICLE_DATEFORMAT
    if current_locale == 'en':
        dateformat_to_use = IMPERIAL_DATEFORMAT
    elif current_locale == 'es':
        dateformat_to_use = METRIC_DATEFORMAT

    for article in response['articles']:
        prediction = classify_article(str(article['content']))
        if prediction:
            article['classification'] = "REAL"
        else:
            article['classification'] = "FAKE"

        formatted_date = datetime.strptime(article['publishedAt'], ARTICLE_DATEFORMAT).strftime(dateformat_to_use)
        article['publishedAt'] = formatted_date

    return jsonify(response)

#----------------------------------------------------------------------------#
# Launch.
#----------------------------------------------------------------------------#


# import models

# Default port:
if __name__ == '__main__':
    app.run(debug=True, port=8080)
