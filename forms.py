from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, StringField
from wtforms.validators import DataRequired, EqualTo, Length, ValidationError, Email
import models
from flask_locale import _
# Set your classes here.


class RegisterForm(FlaskForm):
    name = StringField(_('Username'), validators=[DataRequired(), Length(min=6, max=25)])
    email = StringField(_('Email'), validators=[DataRequired(), Email(), Length(min=6, max=40)])
    password = PasswordField(_('Password'), validators=[DataRequired(), Length(min=6, max=40)])
    confirm = PasswordField(_('Repeat Password'), validators=[DataRequired(),EqualTo('password', message='Passwords must match')])

    def validate_username(self, name):
        user = models.User.query.filter_by(name=name.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = models.User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

class LoginForm(FlaskForm):
    name = StringField(_('Username'), [DataRequired()])
    password = PasswordField(_('Password'), [DataRequired()])


class ForgotForm(FlaskForm):
    email = StringField(_('Email'), validators=[DataRequired(), Email(), Length(min=6, max=40)])


class ResetForm(FlaskForm):
    password = StringField(_('Password'), validators=[DataRequired(), Length(min=6, max=40)])
    confirm = PasswordField(_('Repeat Password'), validators=[DataRequired(),EqualTo('password', message='Passwords must match')])


class ArticleSearchForm(FlaskForm):
    search = StringField(_('Keyword'), validators=[DataRequired()])

class ExploreForm(FlaskForm):
    explore = StringField('Article')

# class EditProfileForm(FlaskForm):
#     name = StringField('Username', validators=[DataRequired()])
#     about_me = TextAreaField('About me', validators=[Length(min=0, max=140)])
#     submit = SubmitField('Submit')
#
#     def __init__(self, original_username, *args, **kwargs):
#         super(EditProfileForm, self).__init__(*args, **kwargs)
#         self.original_username = original_username
#
#     def validate_username(self, username):
#         if username.data != self.original_username:
#             user = User.query.filter_by(username=self.username.data).first()
#             if user is not None:
#                 raise ValidationError('Please use a different username.')
