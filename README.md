## Fake News

### Quick Start

1: Clone the repo
```
  $ git clone git@bitbucket.org:cmpe280fakenews/webapp.git fake-news
  $ cd fake-news
```

2: Initialize and activate a virtualenv
```
  $ virtualenv --no-site-packages env
  $ source env/bin/activate
```

3: Install the dependencies
```
  $ pip install -r requirements.txt
```


4: Copy the config.py.ex to config.py and fill out the required keys and db uri on config.py
```
  $ cp config.py.ex config.py
```

5: Run the development server
```
  $ python app.py
```

6: Navigate to [http://localhost:8080](http://localhost:8080)
