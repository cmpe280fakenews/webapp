import os

# Grabs the folder where the script runs.
basedir = os.path.abspath(os.path.dirname(__file__))

# Enable debug mode.
DEBUG = True

# Connect to the database
SQLALCHEMY_DATABASE_URI = 'postgresql://<USERNAME>:<PASSWORD>@<HOST>:<PORT>/<DBNAME>'

# News API Key
NEWS_API_KEY = <NEWS API KEY>

# Languages
LANGUAGES = ['en', 'es']
