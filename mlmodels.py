from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer

fakenews_model = joblib.load('./ml/finalized_model.sav')
fakenews_vocab = joblib.load('./ml/vocabulary.sav')

# vectorizer for predictions
vectorizer = CountVectorizer(
    analyzer = 'word',
    lowercase = False,
    vocabulary = fakenews_vocab
)

def classify_article(article_text):
    features = vectorizer.fit_transform(
        [article_text]
    )
    return fakenews_model.predict(features)
